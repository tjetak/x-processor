import head from "lodash-es/head"
import tail from "lodash-es/tail"
import zipObject from "lodash-es/zipObject"

/**
 * Extracts one dimensional table into rows of objects. The keys of the generated objects is either defined by using the
 * data from the first row or passed manually as a parameter.
 * @param range - range to be processed.
 * @param header - array of header values.
 */
export default function extract1D(range: GoogleAppsScript.Spreadsheet.Range, header: any[] | undefined = undefined) {
  let res: any[] = []
  let values: any[][] = range.getValues()
  let rows: any[] = values

  if (!header) {
    if (range.getHeight() < 2) throw Error('Range must have a header.')

    header = head(values)
    rows = tail(values)
  } else {
    if (header.length !== range.getWidth()) throw Error('Header columns insufficient.')
  }

  rows.forEach(row => {
    res.push(zipObject(<any[]>header, row))
  })

  return res
}
