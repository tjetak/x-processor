import extract1D from "./extract1D"

// FOR DEVELOPMENT PURPOSES
// const ss = SpreadsheetApp.getActiveSpreadsheet()
//
// function test() {
//   Logger.log(extract1D(ss.getActiveRange()))
// }

// FOR PRODUCTION PURPOSES
export {extract1D}
