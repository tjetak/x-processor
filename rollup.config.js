import cleanup from 'rollup-plugin-cleanup'
import resolve from 'rollup-plugin-node-resolve'
import typescript from 'rollup-plugin-typescript'

export default {
  input: 'src/index.ts',
  output: {
    dir: 'gas',
    format: 'es'
  },
  treeshake: false,
  plugins: [
    resolve(),
    typescript({
      lib: ["es5", "es6", "dom"],
      target: "es3"
    }),
    cleanup()
  ],
};
